package astanait.edu.kz.ui.news.RecyclerNews;

import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;
import astanait.edu.kz.R;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.NewsViewHolder> {

    private List<News> newsList;


    public NewsAdapter(List<News> newsList) {
        this.newsList = newsList;
    }

    @NonNull
    @Override
    public NewsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new NewsViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.news_row, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull NewsViewHolder holder, int position) {
        News news = newsList.get(position);

        holder.textTitle.setText(news.getTitle());
        holder.text.setText(news.getText());
        Picasso.get().load(news.getImageURL()).into(holder.image);

        boolean isExpanded = newsList.get(position).isExpanded();
        if(isExpanded){
            holder.expandableLayout.setVisibility(View.VISIBLE);
            holder.arrow.animate().setDuration(1500).rotation(180.0f);
        }
        else{
            holder.expandableLayout.setVisibility(View.GONE);
            holder.arrow.animate().setDuration(1000).rotation(0.0f);
        }
    }

    @Override
    public int getItemCount() {
        return newsList.size();
    }

    public class NewsViewHolder extends RecyclerView.ViewHolder {

        private TextView textTitle, text;
        private ImageView image;
        private View arrow;
        private ConstraintLayout expandButton, expandableLayout;


        public NewsViewHolder(@NonNull View itemView) {
            super(itemView);

            text = itemView.findViewById(R.id.text_view_news_text);
            textTitle = itemView.findViewById(R.id.text_view_news_title);
            image = itemView.findViewById(R.id.image_view_news);

            expandableLayout = itemView.findViewById(R.id.expandable_layout);
            expandButton = itemView.findViewById(R.id.expand_button);
            arrow = itemView.findViewById(R.id.view_arrow_down_news);

            expandButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    News news = newsList.get(getAdapterPosition());
                    news.setExpanded(!news.isExpanded());
                    notifyItemChanged(getAdapterPosition());
                }
            });
        }
    }
}
