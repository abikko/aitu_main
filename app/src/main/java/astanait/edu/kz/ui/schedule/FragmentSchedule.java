package astanait.edu.kz.ui.schedule;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import astanait.edu.kz.R;

public class FragmentSchedule extends Fragment {
    // TODO: at the moment, before 2 course in university, you should not create this fragment, because in Microsoft Teams you have a schedule
    public FragmentSchedule() {
        // Required empty public constructor
    }
    public static FragmentSchedule newInstance(String param1, String param2) {
        FragmentSchedule fragment = new FragmentSchedule();
        Bundle args = new Bundle();

        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_schedule, container, false);
    }
}
