package astanait.edu.kz.ui.news.RecyclerNews;

import androidx.annotation.DrawableRes;

public class News {
    private String title,text;
    private String imageURL;
    private boolean expanded;

    public News() {
    }

    public News(String title, String text, String imageURL) {
        this.title = title;
        this.text = text;
        this.imageURL = imageURL;
        this.expanded = false;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public boolean isExpanded() {
        return expanded;
    }

    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }
}
