package astanait.edu.kz.ui.verify;

import androidx.annotation.NonNull;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

import astanait.edu.kz.MainActivity;
import astanait.edu.kz.R;
import in.aabhasjindal.otptextview.OtpTextView;

public class verifyActivity extends BasePhoneVerify implements View.OnClickListener {

    private TextView textWithPhoneNumber;
    private TextView otpResend;
    private OtpTextView otpEditTextView;
    private Button buttonVerifyCode;

    private String phoneNumber;
    private String verificationId;
//    private String code;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify);
        start();
        sendVerificationCode();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.text_view_resend: {

                break;
            }
            case R.id.button_verify_verify_code: {
                String code = otpEditTextView.getOTP();

                if(code.isEmpty() || code.length()<6){
                    Toast.makeText(this,"Write the code...",Toast.LENGTH_LONG).show();
                    return;
                }

                verifyCode(code);
                break;
            }
        }
    }

    private void verifyCode(String code){
        try {
            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId,code);

            signInWithCredential(credential);
        }catch (Exception e){
            Toast.makeText(this,"Verification Code is wrong",Toast.LENGTH_LONG);
        }

    }

    private void signInWithCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    Intent intent = new Intent(verifyActivity.this,MainActivity.class);
                    startActivity(intent);
                    finish();
                }else{
                    Toast.makeText(verifyActivity.this, Objects.requireNonNull(task.getException()).getMessage(),Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void sendVerificationCode(){
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,
                60,
                TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                mCallback
        );
    }

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks
            mCallback = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onCodeSent(@NonNull String s, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);
            verificationId = s;
        }

        @Override
        public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
            String code = phoneAuthCredential.getSmsCode();
            if(code!=null){
                verifyCode(code);
            }
        }

        @Override
        public void onVerificationFailed(@NonNull FirebaseException e) {
            Toast.makeText(verifyActivity.this, e.getMessage(),Toast.LENGTH_LONG).show();
            Log.d("Verification",e.getMessage());
        }
    };

    private void start() {
        mAuth = FirebaseAuth.getInstance();
        phoneNumber = getIntent().getStringExtra("phoneNumber");
        otpResend = findViewById(R.id.text_view_resend);
        otpEditTextView = findViewById(R.id.otp_view);
        buttonVerifyCode = findViewById(R.id.button_verify_verify_code);
        textWithPhoneNumber = findViewById(R.id.text_view_verify_code);
        buttonVerifyCode.setOnClickListener(this);
        otpResend.setOnClickListener(this);
        textWithPhoneNumber.setText("We sent a verify code to the number " + phoneNumber + ",enter it");
        textWithPhoneNumber.setTextSize(14.0f);
    }

    @Override
    protected void onStart() {
        super.onStart();

        if(mAuth.getInstance().getCurrentUser() != null){
            startActivity(new Intent(this,MainActivity.class));
            finish();
        }
    }
}