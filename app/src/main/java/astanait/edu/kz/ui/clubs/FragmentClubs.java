package astanait.edu.kz.ui.clubs;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import astanait.edu.kz.R;
import astanait.edu.kz.ui.clubs.RecyclerClubs.Clubs;
import astanait.edu.kz.ui.clubs.RecyclerClubs.ClubsAdapter;

public class FragmentClubs extends Fragment {
    //TODO: you need to create a list of clubs from Realtime Database.*Firebase*
    private ArrayList<Clubs> clubsList;
    private RecyclerView recyclerView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =inflater.inflate(R.layout.fragment_clubs, container, false);

        clubsList = new ArrayList<>();

        clubsList.add(new Clubs("abyl","abyl",R.drawable.aitulogo));

        recyclerView = (RecyclerView) v.findViewById(R.id.recycler_view_clubs);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(new ClubsAdapter(clubsList));


        return v;
    }
}
