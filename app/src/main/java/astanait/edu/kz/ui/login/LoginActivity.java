package astanait.edu.kz.ui.login;

import androidx.annotation.NonNull;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;

import astanait.edu.kz.MainActivity;
import astanait.edu.kz.R;
import astanait.edu.kz.ui.BaseActivity;
import astanait.edu.kz.ui.verify.BasePhoneVerify;
import astanait.edu.kz.ui.verify.verifyActivity;

public class LoginActivity extends BasePhoneVerify implements View.OnClickListener{

    private final static String TAG = "LoginActivity";
    private Button buttonLogin;
    private EditText editTextCountryNumber;
    private EditText editTextPhoneNumber;
    private FirebaseAuth mAuth;
    private TextView textViewForgotPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        start();
    }

    @Override
    public void onClick(@NonNull View v) {
        switch (v.getId()){
            case R.id.button_send_code:{
                sendCode(editTextPhoneNumber,editTextCountryNumber);
                finish();
            }
        }
    }
    private void start(){
        buttonLogin = findViewById(R.id.button_send_code);
        editTextCountryNumber = findViewById(R.id.edit_text_login_country_number);
        editTextPhoneNumber = findViewById(R.id.edit_text_login_phone_number);
        textViewForgotPassword = findViewById(R.id.text_view_terms_of_use);
        mAuth = FirebaseAuth.getInstance();
        buttonLogin.setOnClickListener(this);
        textViewForgotPassword.setOnClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();

        if(FirebaseAuth.getInstance().getCurrentUser() != null){
            startActivity(new Intent(this, MainActivity.class));
        }
    }
}
