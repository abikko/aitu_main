package astanait.edu.kz.ui;

import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

//TODO:
// Because for fragment, and for activity we need to switch the toolbar titles, i created the two classes.
// So in this class you need to add some functions.

public class BaseActivity extends AppCompatActivity {
    protected void setTitleToolbar(@NonNull TextView titleToolbar, String text){
        if (titleToolbar != null) {
            titleToolbar.setText(text);
        }
        else{
            return;
        }
    }
}
