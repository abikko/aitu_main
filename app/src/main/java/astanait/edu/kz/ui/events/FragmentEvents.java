package astanait.edu.kz.ui.events;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import astanait.edu.kz.R;
import astanait.edu.kz.ui.BaseFragment;
public class FragmentEvents extends Fragment {
    //TODO: you need to find a libs for Calendar(maybe something else)
    public FragmentEvents() {
        // Required empty public constructor
    }
    public static FragmentEvents newInstance(String param1, String param2) {
        FragmentEvents fragment = new FragmentEvents();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_events, container, false);
    }
}
