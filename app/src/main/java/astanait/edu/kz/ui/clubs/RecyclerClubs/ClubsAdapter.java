package astanait.edu.kz.ui.clubs.RecyclerClubs;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import astanait.edu.kz.R;

public class ClubsAdapter extends RecyclerView.Adapter<ClubsAdapter.ClubsViewHolder> {

    protected List<Clubs> clubsList;

    public ClubsAdapter(List<Clubs> clubsList) {
        this.clubsList = clubsList;
    }

    @NonNull
    @Override
    public ClubsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.clubs_row,parent,false);

        return new ClubsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ClubsViewHolder holder, int position) {
        Clubs clubs = clubsList.get(position);

        holder.clubName.setText(clubs.getTitle());
        holder.clubLogo.setImageResource(clubs.getImageURL());
        holder.text.setText(clubs.getText());

        boolean isExpanded = clubs.isExpanded();
        holder.expandableLayout.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
    }

    @Override
    public int getItemCount() {
        return clubsList.size();
    }

    protected class ClubsViewHolder extends RecyclerView.ViewHolder {

        ImageView clubLogo;
        TextView text, clubName;
        ConstraintLayout expandableLayout, expandButton;

        public ClubsViewHolder(@NonNull View itemView) {
            super(itemView);

            clubLogo = itemView.findViewById(R.id.image_view_club_logo);
            text = itemView.findViewById(R.id.text_view_clubs_text);
            clubName = itemView.findViewById(R.id.text_view_clubs_title);
            expandableLayout = itemView.findViewById(R.id.expandable_layout_club);
            expandButton = itemView.findViewById(R.id.expand_button_clubs);

            expandButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Clubs clubs = clubsList.get(getAdapterPosition());
                    clubs.setExpanded(!clubs.isExpanded());
                    notifyItemChanged(getAdapterPosition());
                }
            });
        }
    }
}
