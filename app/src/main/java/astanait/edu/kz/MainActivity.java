package astanait.edu.kz;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;


import astanait.edu.kz.ui.BaseActivity;
import astanait.edu.kz.ui.BaseFragment;
import astanait.edu.kz.ui.clubs.FragmentClubs;
import astanait.edu.kz.ui.events.FragmentEvents;
import astanait.edu.kz.ui.login.LoginActivity;
import astanait.edu.kz.ui.news.FragmentNews;
import astanait.edu.kz.ui.schedule.FragmentSchedule;

public class MainActivity extends BaseActivity {
    private BottomNavigationView bnve;
    public TextView toolbarTextView;
    private Toolbar toolbar;

    private FirebaseAuth mAuth;
//    public RecyclerView recyclerViewNews;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mAuth = FirebaseAuth.getInstance();

        toolbar = findViewById(R.id.toolbar);
        toolbarTextView = findViewById(R.id.toolbar_text);
        setTitleToolbar(toolbarTextView, getString(R.string.events));
        setSupportActionBar(toolbar);
        toolbar.setTitle("");

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_main,
                new FragmentEvents()).commit();

        bnve = findViewById(R.id.bnve);
        bnve.setOnNavigationItemSelectedListener(navListener);


    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                    Fragment nextFragment = null;
                    switch (menuItem.getItemId()) {
                        case R.id.menu_event: {
                            nextFragment = new FragmentEvents();
                            setTitleToolbar(toolbarTextView, getString(R.string.events));
                            break;
                        }
                        case R.id.menu_news: {
                            nextFragment = new FragmentNews();
                            setTitleToolbar(toolbarTextView, getString(R.string.news));
                            break;
                        }
                        case R.id.menu_clubs: {
                            nextFragment = new FragmentClubs();
                            setTitleToolbar(toolbarTextView, getString(R.string.clubs));
                            break;
                        }
                        case R.id.menu_schedule: {
                            nextFragment = new FragmentSchedule();
                            setTitleToolbar(toolbarTextView, getString(R.string.schedule));
                        }
                    }

                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_main,
                            nextFragment).commit();
                    return true;
                }
            };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_toolbar,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.logout:{
                mAuth.signOut();
                startActivity(new Intent(this, LoginActivity.class));
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
