package astanait.edu.kz.ui.verify;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import astanait.edu.kz.ui.BaseActivity;

public class BasePhoneVerify extends BaseActivity {
    protected void sendCode(EditText editTextNumber,EditText editTextCountryCode){
        String countryCode = editTextCountryCode.getText().toString().trim();

        String number = editTextNumber.getText().toString().trim();

        if(number.isEmpty()){
            editTextNumber.setError("Number is required");
            editTextNumber.requestFocus();
            return;
        }
        else if(number.length() > 10){
            editTextNumber.setError("Number is length over than 10");
            editTextNumber.requestFocus();
            return;
        }
        else if(number.length() < 10){
            editTextNumber.setError("Number is length less than 10");
            editTextNumber.requestFocus();
            return;
        }

        String phoneNumber = countryCode + number;
        Log.d("SendCode","Code sent to number " + phoneNumber);
        Intent intent = new Intent(this,verifyActivity.class);
        intent.putExtra("phoneNumber",phoneNumber);
        startActivity(intent);
        finish();
    }

    protected boolean isEmpty(View view){
        return view == null;
    }
}
