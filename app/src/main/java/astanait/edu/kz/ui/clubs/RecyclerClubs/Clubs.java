package astanait.edu.kz.ui.clubs.RecyclerClubs;

public class Clubs {
    private String title,text;
    private int imageURL;
    private boolean expanded;

    public Clubs(String title, String text, int imageURL) {
        this.title = title;
        this.text = text;
        this.imageURL = imageURL;
        this.expanded = false;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getImageURL() {
        return imageURL;
    }

    public void setImageURL(int imageURL) {
        this.imageURL = imageURL;
    }

    public boolean isExpanded() {
        return expanded;
    }

    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }
}
