package astanait.edu.kz.ui;

import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.google.firebase.auth.FirebaseAuth;

import astanait.edu.kz.R;
// this class includes the setting toolbar title, and some functions in future ;)
// TODO: realize the class
public class BaseFragment extends Fragment{
    protected void setTitle(String title, TextView textView){
        textView.setText(title);
    }

    protected boolean isAdmin(FirebaseAuth mAuth){
        if(mAuth.getCurrentUser() == null){
            return false;
        }
        return mAuth.getCurrentUser().getPhoneNumber().toString().equals("+77479094214");
    }
}
